<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ActivityLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ActivityLogController extends Controller
{
    public function CreateLogRecord($user_id, $type, $params = null)
    {

        $description = '';
        $now = Carbon::now('Pacific/Auckland')->format('Y-m-d H:i:s');

        switch ($type) {
            case 'create-product':
                $description = 'Created product (ID - ' . $params . ') at ' . $now;
                break;
            case 'update-product':
                $description = 'Updated product (ID - ' . $params . ') at ' . $now;
                break;
            case 'csv-import':
                $description = 'Products Imported with CSV File at ' . $now;
                break;
            case 'permission_create':
                $description = 'Permission Created at ' . $now;
                break;
            case 'permission_update':
                $description = 'Permission Updated at ' . $now;
                break;
            case 'permission_delete':
                $description = 'Permission Deleted at ' . $now;
                break;
            case 'permission_mass_delete':
                $description = 'Permission Mass Deleted at ' . $now;
                break;
            case 'role_create':
                $description = 'Role Created at ' . $now;
                break;
            case 'role_update':
                $description = 'Role Updated at ' . $now;
                break;
            case 'role_delete':
                $description = 'Role Deleted at ' . $now;
                break;
            case 'role_mass_delete':
                $description = 'Role Mass Deleted at ' . $now;
                break;
            case 'website_update':
                $description = 'Published data for Website at ' . $now;
                break;
            case 'supplier_update':
                $description = 'Publied data for Suppliers at ' . $now;
                break;
            case 'create_user':
                $description = 'User Created at ' . $now;
                break;
            case 'update_user':
                $description = 'User Updated at ' . $now;
                break;
            case 'delete_user':
                $description = 'User Deteted at ' . $now;
                break;
            case 'mass_delete_user':
                $description = 'Users Mass Deleted at ' . $now;
                break;
            case 'change_password':
                $description = 'Password changed for ' . $params . ' at ' . $now;
                break;
        }

        $activity = new ActivityLog();
        $activity->user_id = $user_id;
        $activity->description = $description;
        $activity->created_by = $user_id;
        $activity->created_at = $now;
        $activity->save();
    }

    public function view()
    {
        if (Gate::allows('admin_activity_log')) {
            $activities = ActivityLog::orderBy('created_at', 'DESC')->with(['user'])->get();
        } elseif (Gate::allows('user_activity_log')) {
            $activities = ActivityLog::orderBy('created_at', 'DESC')->where('user_id', Auth::user()->id)->with(['user'])->limit(25)->get();
        } else {
            return abort(401);
        }
        // $activities = ActivityLog::where()->get()
        return view('admin.activity_log')
            ->with('activities', $activities);
    }
}
