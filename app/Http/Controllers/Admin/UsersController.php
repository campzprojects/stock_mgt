<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }

        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSupplier()
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.create_supplier', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsersRequest $request)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        if ($request->supplier_code){
            $res = User::where('supplier_code',$request->supplier_code)->count();
            if ($res > 0){
                session()->flash('error', 'Supplier code "'.$request->supplier_code.'"already exists!');
                return redirect()->back();
            }
        }
        $user = User::create($request->all());

        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        if (isset($request->supplier_code) or in_array('supplier', $roles)) {
            $user->supplier_code = isset($request->supplier_code) ? $request->supplier_code : $this->generateSupplierCode($user);
            $user->save();
        }
        app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'create_user');
        return redirect()->route('admin.users.index');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editSupplier(User $user)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        $roles = Role::get()->pluck('name', 'name');

        return view('admin.users.edit_supplier', compact('user', 'roles'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, User $user)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        if ($request->supplier_code){
            $res = User::where('supplier_code',$request->supplier_code)->where('id','!=',$user->id)->count();
            if ($res > 0){
                session()->flash('error', 'Supplier code "'.$request->supplier_code.'"already exists!');
               return redirect()->back();
            }
        }

        $user->update($request->all());
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);
        app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'update_user');
        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }

        $user->load('roles');

        return view('admin.users.show', compact('user'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }

        $user->delete();
        app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'delete_user');
        return redirect()->route('admin.users.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        User::whereIn('id', request('ids'))->delete();
        app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'mass_delete_user');
        return response()->noContent();
    }

    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function listSuppliers()
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }
        $supplier_code = request()->supplier_code;

        if(!empty($supplier_code)){
            $users = User::where('supplier_code',$supplier_code)
            ->orWhere('name', 'like', '%'.$supplier_code.'%')->get();
        }else{
            $users = User::all();
        }

        return view('admin.users.suppiers', compact('users'));
    }

    public function checkAuth()
    {
        if (Gate::allows('users_manage')) {
            return true;
        } elseif (Gate::allows('user_add')) {
            return true;
        } elseif (Gate::allows('list_suppliers')) {
            return true;
        } else {
            return false;
        }
    }

    public function generateSupplierCode($user)
    {
        if (!empty($user->name)) {
            $name = strtoupper(substr(str_replace(' ', '', $user->name), 0, 2));
            $num = $invID = str_pad($user->id, 4, '0', STR_PAD_LEFT);
            return $name . $num;
        }
    }

    public function changeStatus(Request $request)
    {
        if (!$this->checkAuth()) {
            return abort(401);
        }

        if ($request->status == 'deactivate') {
            return User::where('id',$request->id)->update(['status'=>0]);
        }else{
            return User::where('id',$request->id)->update(['status'=>1]);
        }
    }
}
