<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function productSave(ProductRequest $request)
    {
        $res = $this->addSingleProduct($request);

        if ($res) {
            session()->flash('message', "Product added successfully!");
        } else {
            session()->flash('error', "Product ID already exists!");
        }
        return redirect()->back();
    }

    public function addSingleProduct($request)
    {
        $product_image = null;
        if ($request->hasFile('product_image')) {
            $product_image = $this->uploadImage($request);
        }
        $product_id = strtoupper(str_replace(' ', '', $request['product_id']));
        $checkProductId = Product::where('product_id', $product_id)->count();
        if ($checkProductId) {
            return false;
        }

        $product = new Product();
        $product->supplier_code = $request['supplier_code'];
        $product->product_id = $product_id;
        $product->product_name = $request['product_name'];
        $product->stock_count = $request['stock_count'];
        $product->product_image = $product_image;
        $product->price = $request['price'];
        
        if ($product->stock_count_old != $product->stock_count) {
            $product->is_changed = 1;
            $this->updateSupplierStatus($request['supplier_code'], 1);
        } else {
            $product->is_changed = 0;
        }
        $res = $product->save();

        if ($res) {
            app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'create-product', $product->product_id);
        }

        return $res;
    }

    public function uploadImage($request)
    {
        $path = 'images/products/' . $request->supplier_code;
        $name = $request->supplier_code . '-' . time() . '.' . $request->file('product_image')->extension(); //create file name
        $request->file('product_image')->move('./' . $path, $name); //path to save file
        return $path . '/' . $name;
    }

    public function updateSupplierStatus($supplierCode, $status)
    {
        User::where('supplier_code', $supplierCode)->update(['is_changed' => $status]);
    }

    public function productSingle(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        return response()->json($product);
    }

    public function productUpdate(Request $request)
    {
        $product_image = '';

        if ($request->hasFile('product_image')) {
            $product_image = $this->uploadImage($request);
            if ($request->product_image_old) {
                if (file_exists(public_path($request->product_image_old))) {
                    unlink(public_path($request->product_image_old));
                }
            }
        } else {
            if ($request->product_image_old) {
                if (file_exists(public_path($request->product_image_old))) {
                    $product_image = $request->product_image_old;
                } else {
                    $product_image = null;
                }
            } else {
                $product_image = null;
            }
        }
        // dd($product_image);
        $product_id = strtoupper(str_replace(' ', '', $request['product_id']));
        // $checkProductId = Product::where('product_id', $product_id)->where('id','!=',$request['id'])->count();

        // if ($checkProductId) {
        //     session()->flash('error', "Product ID is alreay exist!");
        //     return redirect()->back();
        // }

        $product = Product::where('id', $request->id)->first();
        $product->supplier_code = $request['supplier_code'];
        $product->product_id = $product_id;
        $product->product_name = $request['product_name'];
        $product->product_image = $product_image;
        $product->price = $request['price'];

        if ($product->is_changed == 0) {
            if ($product->stock_count_old != $request->stock_count) {
                $product->is_changed = 1;
                $product->website_updated = 0;
                $this->updateSupplierStatus($request['supplier_code'], 1);
            } else {
                $product->is_changed = 0;
                $product->website_updated = 1;
            }
            $product->stock_count_old = $product->stock_count;
        } else {
            if ($product->stock_count_old != $product->stock_count) {
                $product->is_changed = 1;
                $product->website_updated = 0;
                $this->updateSupplierStatus($request['supplier_code'], 1);
            }
        }
        $product->stock_count = $request['stock_count'];
        $res = $product->save();
        if ($res) {
            app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'update-product', $product->product_id);
            session()->flash('message', "Product updated successfully!");
        } else {
            session()->flash('error', "Product not updated!");
        }
        return redirect()->back();
    }

    public function productStockCountUpdate()
    {
        $request = request();
        $product = Product::where('id', $request->id)->first();

        if ($product->is_changed == 0) {
            if ($product->stock_count_old != $request->stock_count) {
                $product->is_changed = 1;
                $product->website_updated = 0;
                $this->updateSupplierStatus($request['supplier_code'], 1);
            } else {
                $product->is_changed = 0;
                $product->website_updated = 1;
            }
            $product->stock_count_old = $product->stock_count;
        } else {
            if ($product->stock_count_old != $product->stock_count) {
                $product->is_changed = 1;
                $product->website_updated = 0;
                $this->updateSupplierStatus($request['supplier_code'], 1);
            }
        }
        $product->stock_count = $request['stock_count'];
        $res = $product->save();
        if ($res) {
            app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'update-product', $product->product_id);
            session()->flash('message', "Product updated successfully!");
        } else {
            session()->flash('error', "Product not updated!");
        }
        return redirect()->back();
    }

    public function checkProductId()
    {
        $request = request();
        $product = Product::where('product_id', strtoupper($request->product_id))->count();
        if($product){
            return response()->json(false);
        }else{
            return response()->json(true);
        }
        
    }
}
