<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class SupplierController extends Controller
{
    //
    public function supplierSingle($id)
    {
        $currentSupplier = User::where('id', $id)->first();
        $suppliers = User::whereNotNull('supplier_code')
            ->select('id', 'name', 'supplier_code', 'email')->get();

        $products = Product::where('supplier_code',$currentSupplier->supplier_code)
            ->orderBy('is_changed','desc')
            ->get();

        $toBePublishedSupplier = Product::where('supplier_code',$currentSupplier->supplier_code)
            ->where('show_supplier',0)
            ->count();

        $toBePublishedWebsite = Product::where('supplier_code',$currentSupplier->supplier_code)
            ->where('website_updated',0)
            ->count();

            if (Gate::allows('add_product')) {
                $readOnly = '';
            } else {
                $readOnly = 'readonly';
            }
        return view('admin.supplier.data-population')
            ->with('readOnly',$readOnly)
            ->with('suppliers', $suppliers)
            ->with('currentSupplier', $currentSupplier)
            ->with('products', $products)
            ->with('toBePublishedSupplier', $toBePublishedSupplier)
            ->with('toBePublishedWebsite', $toBePublishedWebsite);
    }

    public function getAllProductsBySupplier($currentSupplier)
    {
    }

    public function websiteUpdate(Request $request){
        $products = Product::where('supplier_code',$request->supplier_code)
            ->where('website_updated',0)->get();

        foreach ($products as $product){
            $product->stock_count_old = $product->stock_count;
            $product->is_changed = 0;
            $product->website_updated = 1;
            $product->save();
        }

        $res = count($products);

        if ($res){
            app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'website_update');
            User::where('is_changed', 1)->update(['is_changed'=>0]);
            return response()->json(['status'=>true, 'count'=>$res]);
        }else{
            return response()->json(['status'=>false, 'count'=>$res]);
        }
    }
    public function supplierUpdate(Request $request){
        $res = Product::where('supplier_code',$request->supplier_code)
            ->where('show_supplier',0)->update(['show_supplier'=>1]);

        if ($res){
            app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'supplier_update');
            return response()->json(['status'=>true, 'count'=>$res]);
        }else{
            return response()->json(['status'=>false, 'count'=>$res]);
        }
    }
    
}
