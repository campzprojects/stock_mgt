<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\ProductsImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function import()
    {
        $res = Excel::import(new ProductsImport,request()->file('file'));
        session()->flash('message', "CSV Import successful!");
        app(ActivityLogController::class)->CreateLogRecord(Auth::user()->id, 'csv-import');
        return back();
    }
}
