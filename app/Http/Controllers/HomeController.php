<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if(!empty($user->supplier_code) AND !$user->t_c_acknowledged){
            return redirect()->route('admin.termsAndConditions');
        }
        return view('home');
    }

    public function termsAndConditions()
    {
        $user = Auth::user();
        return view('tc')->with('user',$user);
    }
    
    public function saveTermsAndConditions()
    {
       $request = request();

        $res = User::where('id',$request->user_id)->update(['t_c_acknowledged'=>1]);

        if ($res) {
            session()->flash('message', "You have acknowleged the terms and conditions successfully!");
            return redirect()->route('admin.home');
        } else {
            session()->flash('error', "something went wrong!");
        }
        
    }
}
