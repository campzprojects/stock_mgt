<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supplier_code' => 'required',
            'product_id' => 'required|unique:products,product_id,',
            'product_name' => 'required',
            'product_image' => 'mimes:jpg,png',
            'stock_count' => 'required|numeric|min:0',
        ];
    }
}
