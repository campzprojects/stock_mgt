<?php
Route::redirect('/', 'admin/home');

Auth::routes(['register' => false]);

Route::group(['middleware' => ['checkstatus']], function () {
    Route::post('/login', 'Auth\LoginController@login');
});

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('termsAndConditions');
    Route::post('/terms-and-conditions', 'HomeController@saveTermsAndConditions')->name('saveTermsAndConditions');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::delete('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::delete('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::post('users/status/change', 'Admin\UsersController@changeStatus');
    Route::get('users/supplier/create', 'Admin\UsersController@createSupplier')->name('supplier.create');
    Route::get('users/supplier/{user}/edit', 'Admin\UsersController@editSupplier')->name('supplier.edit');
    Route::get('user/suppliers', 'Admin\UsersController@listSuppliers')->name('supplier.list');
    Route::get('user/supplier/view/{id}', 'Admin\SupplierController@supplierSingle')->name('supplier.single');
    Route::delete('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');

    Route::post('update/website', 'Admin\SupplierController@websiteUpdate')->name('update.website');
    Route::post('update/supplier', 'Admin\SupplierController@supplierUpdate')->name('update.supplier');

    //product
    Route::post('product/save', 'Admin\ProductController@productSave')->name('product.save');    
    Route::post('product/update', 'Admin\ProductController@productUpdate')->name('product.update');
    Route::post('product/single/get', 'Admin\ProductController@productSingle')->name('product.single.get');
    Route::post('product/single/stockcount/update', 'Admin\ProductController@productStockCountUpdate')->name('product.stockcount.update');
    // check product id
    Route::post('/check/product-id', 'Admin\ProductController@checkProductId')->name('product_id.check');
    //    CSV import
    Route::post('import/csv', 'Admin\ImportController@import')->name('csv.import');

    // activity log
    Route::get('activitylog/view', 'Admin\ActivityLogController@view')->name('activitylog.view');

    
    
    // settings
    Route::get('settings/general', 'Admin\SettingsController@index')->name('settings.general');
    Route::post('settings/general/save', 'Admin\SettingsController@save')->name('settings.general.save');

});
