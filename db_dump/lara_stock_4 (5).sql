-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2021 at 05:31 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lara_stock_4`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `user_id`, `description`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'Created product (ID - ASSADASDS) at 2021-09-24 13:00:47', 1, '2021-09-24 13:00:47', '2021-09-24 13:00:47'),
(2, 1, 'Updated product (ID - ASSADASDS) at 2021-09-24 13:04:51', 1, '2021-09-24 13:04:51', '2021-09-24 13:04:51'),
(3, 1, 'Updated product (ID - SDFSDFDSSWSD) at 2021-09-24 13:07:42', 1, '2021-09-24 13:07:42', '2021-09-24 13:07:42'),
(4, 1, 'Products Imported with CSV File at 2021-09-24 13:35:06', 1, '2021-09-24 13:35:06', '2021-09-24 13:35:06'),
(5, 1, 'Permission Created at 2021-09-24 13:43:09', 1, '2021-09-24 13:43:09', '2021-09-24 13:43:09'),
(6, 1, 'Permission Updated at 2021-09-24 13:43:27', 1, '2021-09-24 13:43:27', '2021-09-24 13:43:27'),
(7, 1, 'Permission Deleted at 2021-09-24 13:43:32', 1, '2021-09-24 13:43:32', '2021-09-24 13:43:32'),
(8, 1, 'Permission Created at 2021-09-24 14:03:58', 1, '2021-09-24 14:03:58', '2021-09-24 14:03:58'),
(9, 1, 'Permission Created at 2021-09-24 14:04:18', 1, '2021-09-24 14:04:18', '2021-09-24 14:04:18'),
(10, 1, 'Role Mass Deleted at 2021-09-24 14:07:19', 1, '2021-09-24 14:07:19', '2021-09-24 14:07:19'),
(11, 1, 'Permission Deleted at 2021-09-24 14:18:54', 1, '2021-09-24 14:18:54', '2021-09-24 14:18:54'),
(12, 1, 'Role Mass Deleted at 2021-09-24 14:25:17', 1, '2021-09-24 14:25:17', '2021-09-24 14:25:17'),
(13, 1, 'Permission Updated at 2021-09-24 15:11:12', 1, '2021-09-24 15:11:12', '2021-09-24 15:11:12'),
(14, 1, 'Permission Created at 2021-09-24 15:11:24', 1, '2021-09-24 15:11:24', '2021-09-24 15:11:24'),
(15, 1, 'Role Mass Deleted at 2021-09-24 15:24:23', 1, '2021-09-24 15:24:23', '2021-09-24 15:24:23'),
(16, 1, 'Role Mass Deleted at 2021-09-24 15:28:54', 1, '2021-09-24 15:28:54', '2021-09-24 15:28:54'),
(17, 1, 'Role Mass Deleted at 2021-09-24 15:29:07', 1, '2021-09-24 15:29:07', '2021-09-24 15:29:07'),
(18, 1, 'Role Mass Deleted at 2021-09-24 15:29:13', 1, '2021-09-24 15:29:13', '2021-09-24 15:29:13'),
(19, 9, 'Created product (ID - ASDSSD) at 2021-09-24 15:30:29', 9, '2021-09-24 15:30:29', '2021-09-24 15:30:29');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_07_12_145959_create_permission_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 7),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(3, 'App\\User', 3),
(3, 'App\\User', 8),
(4, 'App\\User', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'users_manage', 'web', '2021-08-21 10:01:17', '2021-08-21 10:01:17'),
(2, 'add_supplier', 'web', '2021-08-21 10:04:03', '2021-08-21 10:04:03'),
(3, 'add_data_entry', 'web', '2021-08-21 10:04:18', '2021-08-21 10:04:18'),
(4, 'submit_website', 'web', '2021-08-21 10:05:34', '2021-08-21 10:05:34'),
(5, 'add_stock', 'web', '2021-08-21 10:06:10', '2021-08-21 10:06:10'),
(6, 'update_stock', 'web', '2021-08-21 10:06:19', '2021-08-21 10:06:19'),
(7, 'user_add', 'web', '2021-08-21 21:08:30', '2021-08-21 21:08:30'),
(8, 'admin_dashboard', 'web', '2021-08-21 21:40:30', '2021-08-21 21:47:19'),
(9, 'supplier_dashboard', 'web', '2021-08-21 21:40:43', '2021-08-21 21:40:43'),
(10, 'data_entry_dashboard', 'web', '2021-08-21 21:50:47', '2021-08-21 21:50:47'),
(11, 'manager_dashboard', 'web', '2021-08-21 21:51:23', '2021-08-21 21:51:23'),
(12, 'edit_supplier', 'web', '2021-08-23 11:23:49', '2021-08-23 11:23:49'),
(13, 'publish_data', 'web', '2021-09-23 10:33:34', '2021-09-23 10:33:34'),
(14, 'add_product', 'web', '2021-09-23 10:34:33', '2021-09-23 10:34:33'),
(15, 'import_csv', 'web', '2021-09-23 10:35:06', '2021-09-23 10:35:06'),
(16, 'edit_product', 'web', '2021-09-23 10:37:09', '2021-09-23 10:37:09'),
(17, 'view_website_update', 'web', '2021-09-23 10:49:46', '2021-09-23 10:49:46'),
(18, 'list_suppliers', 'web', '2021-09-23 11:32:24', '2021-09-23 11:32:24'),
(21, 'user_activity_log', 'web', '2021-09-24 08:34:18', '2021-09-24 09:41:12'),
(22, 'admin_activity_log', 'web', '2021-09-24 09:41:24', '2021-09-24 09:41:24');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock_count_old` float NOT NULL DEFAULT 0,
  `stock_count` float NOT NULL,
  `is_changed` int(11) NOT NULL DEFAULT 1,
  `show_supplier` int(11) NOT NULL DEFAULT 0,
  `website_updated` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `supplier_code`, `product_id`, `product_name`, `product_image`, `stock_count_old`, `stock_count`, `is_changed`, `show_supplier`, `website_updated`, `status`, `created_at`, `updated_at`) VALUES
(4, 'SU0009', 'PRODUCT001', 'das', NULL, 200, 200, 0, 1, 1, 1, '2021-08-24 09:25:59', '2021-09-23 11:05:35'),
(5, 'SU0009', 'PRODUCT002', 'das edit', 'images/products/SU0009/SU0009-1632414778.jpg', 1500, 1500, 0, 1, 1, 1, '2021-08-24 09:36:05', '2021-09-23 11:05:35'),
(7, 'SU0009', 'PRODUCT003', 'product003', 'images/products/SU0009/SU0009-1632414798.jpg', 30000, 300, 1, 1, 0, 1, '2021-08-24 09:47:55', '2021-09-23 11:06:39'),
(8, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 10, 10, 0, 1, 1, 1, '2021-09-18 23:43:26', '2021-09-21 22:10:23'),
(9, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 10, 10, 0, 1, 1, 1, '2021-09-18 23:44:08', '2021-09-21 22:10:23'),
(10, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 10, 10, 0, 1, 1, 1, '2021-09-20 10:51:47', '2021-09-21 22:10:23'),
(11, 'SU0009', 'SAZD', 'ads', 'images/products/SU0009/SU0009-1632487476.jpg', 0, 10, 1, 0, 0, 1, '2021-09-24 07:14:36', '2021-09-24 07:14:36'),
(16, 'SU0009', 'SDFSDFDSSWSD', 'fdsfsdf', 'images/products/SU0009/SU0009-1632488862.jpg', 0, 3200, 1, 0, 0, 1, '2021-09-24 07:30:07', '2021-09-24 07:37:42'),
(17, 'SU0009', 'ASSADASDS', 'dsadsa asdsasa sdsa', 'images/products/SU0009/SU0009-1632488691.jpg', 0, 123, 1, 0, 0, 1, '2021-09-24 07:30:46', '2021-09-24 07:34:51'),
(18, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 07:42:11', '2021-09-24 07:42:11'),
(19, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 07:50:04', '2021-09-24 07:50:04'),
(20, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 07:53:42', '2021-09-24 07:53:42'),
(21, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 08:00:44', '2021-09-24 08:00:44'),
(22, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 08:01:22', '2021-09-24 08:01:22'),
(23, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 08:03:26', '2021-09-24 08:03:26'),
(24, 'SU0009', 'PRODUCT003', 'PRODUCT003', NULL, 0, 10, 1, 0, 0, 1, '2021-09-24 08:05:05', '2021-09-24 08:05:05'),
(25, 'SU0009', 'ASDSSD', 'sdasd asd', NULL, 0, 100, 1, 0, 0, 1, '2021-09-24 10:00:29', '2021-09-24 10:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'web', '2021-08-21 10:01:17', '2021-08-21 10:01:17'),
(2, 'supplier', 'web', '2021-08-21 10:06:39', '2021-08-21 10:06:39'),
(3, 'data Entry', 'web', '2021-08-21 21:10:25', '2021-08-21 21:10:25'),
(4, 'project manager', 'web', '2021-08-21 21:11:15', '2021-08-21 21:11:15');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(2, 4),
(3, 1),
(3, 3),
(3, 4),
(4, 1),
(4, 3),
(4, 4),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 4),
(8, 1),
(9, 2),
(10, 3),
(11, 4),
(12, 1),
(12, 4),
(13, 1),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(14, 4),
(15, 1),
(15, 2),
(15, 3),
(15, 4),
(16, 1),
(16, 2),
(16, 3),
(16, 4),
(17, 4),
(18, 3),
(21, 2),
(21, 3),
(22, 1),
(22, 4);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `index_key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `index_key`, `value`, `created_at`, `updated_at`) VALUES
(1, 'save_activity_log', '1', '2021-09-24 19:33:14', '2021-09-24 19:33:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_changed` int(1) NOT NULL DEFAULT 0,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `supplier_code`, `is_changed`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 0, 'Admin', 'admin@gmail.com', '$2y$10$LFfvtqvNYbWev6NQnGFhE.FYRLBn861w6lNiRHzDz9itPTx4AjBtS', NULL, '2021-08-21 10:01:17', '2021-08-21 10:01:17'),
(2, 'US0002', 0, 'User Supplier', 'supplier@gmail.com', '$2y$10$mlKXai.qCXJspnXrbd7Ase601U6Yw51m0YXlQa/pFP/dnnHBNiCIK', NULL, '2021-08-21 21:09:41', '2021-08-21 21:09:41'),
(3, NULL, 0, 'User Data entry', 'dataentry@gmail.com', '$2y$10$i/ydHFqRovr72GJgjL0BBuKgsQMw3.W45DDeG8PpyV9FEvPYWt5my', NULL, '2021-08-21 21:12:10', '2021-08-21 21:12:10'),
(4, NULL, 0, 'User project manager', 'manager@gmail.com', '$2y$10$NlLZKA9WD6SPlERxgpK26e7.BOGjPLnpdP8RIES/uEP/1jPpjsANK', NULL, '2021-08-21 21:12:51', '2021-08-21 21:12:51'),
(9, 'SU0009', 1, 'supplier 002', 'supplier002@gmail.com', '$2y$10$mnALBYTspasjcmXpPn.ZW.hIWJfik6DDAXOdU6MgoBH94by.blJA.', NULL, '2021-08-23 11:35:57', '2021-09-24 10:00:29'),
(10, 'US0005', 0, 'sa', 'supplier003@gmail.com', '$2y$10$8jtZnyaLRassYRK6FX8dFODaIzPz1yORULsu/GbOrFRFDYLmi0MPu', NULL, '2021-08-23 12:01:09', '2021-08-23 12:01:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
