@extends('layouts.admin')
@section('content')
    <div class="content">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-products">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="15%">User</th>
                            <th width="65%">Description</th>
                            <th width="15%">Action Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($activities))
                            @php $x = 1; @endphp
                            @foreach($activities as $key => $activity)

                        
                            <tr data-entry-id="{{ $activity->id }}">
                                    <td></td>
                                    <td>{{$activity->user->name}} 
                                    @can('admin_activity_log')
                                    <span class="badge badge-primary">{{$activity->user->email}}</span>
                                    @endcan
                                    </td>
                                    <td>{{$activity->description}}</td>
                                    <td>{{$activity->created_at}}</td>
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">No records available</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        var asset_path = "{{asset('')}}";
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

            $.extend(true, $.fn.dataTable.defaults, {
                pageLength: 100,
            });
            $('.datatable-products:not(.ajaxTable)').DataTable({buttons: dtButtons})
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        });

    </script>
@endsection