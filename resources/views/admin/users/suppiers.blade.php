@extends('layouts.admin')
@section('content')
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.supplier.title_singular') }} {{ trans('global.list') }}
            <div class="pull-right">
                <form action="" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" id="" placeholder="Search Suppler here" name="supplier_code" required>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-secondary" style="border-radius: 0px 5px 5px 0px;" type="submit">
                                <i class="fas fa-fw fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body">
            <div class="row">

            @foreach($users as $key => $user)
                @php
                    $visible = in_array('supplier', $user->roles()->pluck('name')->toArray());
                @endphp
                @if($visible == true)
                        <div class="col-3 col-sm-3 col-md-3 mb-3 mb-xl-0">
                            <a href="{{route('admin.supplier.single',['id'=>$user->id])}}"
                               class="btn btn-block btn-lg mt-2 {{$user->is_changed ? 'btn-danger' : 'btn-warning'}}"
                               type="button">{{($user->supplier_code) ? $user->supplier_code : $user->name}}</a>
                        </div>
                @endif
            @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
            @can('users_manage')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('admin.users.mass_destroy') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({selected: true}).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: {ids: ids, _method: 'DELETE'}
                        })
                            .done(function () {
                                location.reload()
                            })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[1, 'desc']],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({buttons: dtButtons})
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        })

    </script>
@endsection