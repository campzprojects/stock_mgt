@extends('layouts.admin')
@section('content')
    @can('users_manage')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                @if (request()->input('type') == null)
                    <a class="btn btn-success addbtn" href="{{ route("admin.users.create") }}">
                        {{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}
                    </a>
                @endif
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                    <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            Code
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                        <th width="2%">
                            Status
                        </th>
                        <th width="2%">
                            &nbsp;
                        </th>
                        <th width="2%">
                            &nbsp;
                        </th>
                        <th width="2%">
                            &nbsp;
                        </th>
                        <th width="2%">
                            &nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $key => $user)
                        @php
                            if (request()->input('type') != null){
                                $visible = in_array(request()->input('type'), $user->roles()->pluck('name')->toArray());
                            }else{
                                $visible = true;
                            }
                        @endphp
                        @if($visible == true)
                            <tr data-entry-id="{{ $user->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $user->id ?? '' }}
                                </td>
                                <td>
                                    {{ $user->supplier_code ?? '-' }}
                                </td>
                                <td>
                                    {{ $user->name ?? '' }}
                                </td>
                                <td>
                                    {{ $user->email ?? '' }}
                                </td>
                                <td>
                                    @foreach($user->roles()->pluck('name') as $role)
                                        <span class="badge badge-info">{{ $role }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @if($user->status)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Deactive</span>
                                    @endif
                                </td>
                                <td>
                                    @if($user->status)
                                        <button class="btn btn-xs btn-danger"
                                                onclick="changeStatus({{$user->id}}, 'deactivate')">
                                            Deactivate
                                        </button>
                                    @else
                                        <button class="btn btn-xs btn-success"
                                                onclick="changeStatus({{$user->id}}, 'activate')">
                                            Activate
                                        </button>
                                    @endif

                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.users.show', $user->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                </td>
                                <td>
                                    @if(empty($user->supplier_code))
                                        <a class="btn btn-xs btn-info"
                                           href="{{ route('admin.users.edit', $user->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @else
                                        <a class="btn btn-xs btn-info"
                                           href="{{route('admin.supplier.edit',$user->id)}}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST"
                                          onsubmit="return confirm('{{ trans('global.areYouSure') }}');"
                                          style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger"
                                               value="{{ trans('global.delete') }}">
                                    </form>

                                </td>

                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>


        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
                    @can('users_manage')
            let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
            let deleteButton = {
                text: deleteButtonTrans,
                url: "{{ route('admin.users.mass_destroy') }}",
                className: 'btn-danger',
                action: function (e, dt, node, config) {
                    var ids = $.map(dt.rows({selected: true}).nodes(), function (entry) {
                        return $(entry).data('entry-id')
                    });

                    if (ids.length === 0) {
                        alert('{{ trans('global.datatables.zero_selected') }}')

                        return
                    }

                    if (confirm('{{ trans('global.areYouSure') }}')) {
                        $.ajax({
                            headers: {'x-csrf-token': _token},
                            method: 'POST',
                            url: config.url,
                            data: {ids: ids, _method: 'DELETE'}
                        })
                            .done(function () {
                                location.reload()
                            })
                    }
                }
            }
            dtButtons.push(deleteButton)
            @endcan

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[1, 'asc']],
                pageLength: 100,
            });
            $('.datatable-User:not(.ajaxTable)').DataTable({buttons: dtButtons})
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        });


        function changeStatus(id, status) {
            Swal.fire({
                title: 'Do you wish ' + status + ' this user?',
                text: '',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No'
            }).then((result) = > {
                /* Read more about isConfirmed, isDenied below */
                if(result.isConfirmed
        )
            {
                $.ajax({
                    headers: {'x-csrf-token': _token},
                    method: 'POST',
                    url: '/admin/users/status/change',
                    data: {id: id, status: status}
                })
                    .done(function (res) {
                        console.log(res);
                        if (res) {
                            var msg = 'User ' + status + ' successfully!';
                            $('#js_msg').html('<div class="alert alert-success" role="alert">' + msg + '</div>');

                            setTimeout(function (e) {
                                location.reload();
                            }, 1500);
                        }
                    })
            }
        })
            ;
        }


    </script>
@endsection