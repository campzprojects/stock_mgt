@extends('layouts.admin')
@section('content')
@can('import_csv')

@endcan
    <div class="content">
        <div class="row">
            <div class="col-6 col-sm-3 col-md-2 col-xl mb-3 mb-xl-0">
                @can('import_csv')
                    <button class="btn btn-block btn-primary"
                            type="button" data-toggle="modal" data-target="#import_csv">Import CSV/Excel
                    </button>
                @endcan
            </div>
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                @can('add_product')
                    <button class="btn btn-block btn-success"
                            type="button" data-toggle="modal" data-target="#add_product">Add Product
                    </button>
                @endcan
            </div>
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                @can('edit_supplier')
                    <a href="{{route('admin.supplier.edit',$currentSupplier->id)}}"
                       class="btn btn-block btn-success" type="button">Edit Supplier Details</a>
                @endcan
            </div>
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                @can('submit_website')
                    <a href="#"
                       class="btn btn-block {{$toBePublishedWebsite == 0 ? 'disabled btn-warning' : 'btn-danger'}}" type="button"
                       title="{{$toBePublishedWebsite}} {{Str::plural('record', $toBePublishedWebsite)}} to be updated"
                       {{$toBePublishedWebsite == 0 ? 'aria-disabled="true"' : ''}}
                       onclick="websiteUpdate({{$toBePublishedWebsite}},'{{$currentSupplier->supplier_code}}')">Website Update</a>
                @endcan
            </div>
            <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                @can('publish_data')
                    <a href="#"
                       class="btn btn-block btn-warning {{$toBePublishedSupplier == 0 ? 'disabled' : ''}}" type="button"
                       title="{{$toBePublishedSupplier}} {{Str::plural('record', $toBePublishedSupplier)}} to be updated"
                       {{$toBePublishedSupplier == 0 ? 'aria-disabled="true"' : ''}}
                       onclick="supplierUpdate({{$toBePublishedSupplier}},'{{$currentSupplier->supplier_code}}')">Publish Data</a>
                @endcan
            </div>
        </div>
        <br>

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class=" table table-bordered table-striped table-hover datatable datatable-products">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="15%">Supplier Code</th>
                            <th width="15%">Product ID</th>
                            <th width="15%">Product Photo</th>
                            <th width="25%">Product Name</th>
                            <th width="15%">Stock Count</th>
                            {{-- <th width="8%">Price</th> --}}
                            @can('edit_product')     
                                <th width="10%">Action</th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($products))
                            @php $x = 1; @endphp
                            @foreach($products as $key => $product)

                                @can('view_website_update')
                                    <tr data-entry-id="{{ $product->id }}"
                                        class="{{(!$product->website_updated) ? 'web_changed' : ''}}">
                                @else
                                    <tr data-entry-id="{{ $product->id }}"
                                        class="{{($product->is_changed) ? 'row_changed' : ''}}">
                                        @endcan

                                        <td></td>
                                        <td>{{$product->supplier_code}}</td>
                                        <td>{{$product->product_id}}</td>
                                        <td>

                                            @if(!empty($product->product_image))
                                                @if (file_exists(public_path($product->product_image)))

                                                    <a class="lightbox" href="#{{$product->product_image}}">
                                                        <img src="{{asset('')}}{{$product->product_image}}"/>
                                                    </a>
                                                    <div class="lightbox-target" id="{{$product->product_image}}">
                                                        <img src="{{asset('')}}{{$product->product_image}}"/>
                                                        <a class="lightbox-close" href="#"></a>
                                                    </div>
                                                @else
                                                    <img src="{{asset('')}}images/default.jpg" alt="" width="75px">
                                                @endif
                                            @else
                                                <img src="{{asset('')}}images/default.jpg" alt="" width="75px">
                                            @endif
                                    </td>
                                    <td>{{$product->product_name}}</td>
                                    <td>
                                        {{-- {{$product->stock_count}} --}}
                                        <form action="{{route('admin.product.stockcount.update')}}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$product->id}}">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" placeholder="Stock Count" 
                                                aria-label="" aria-describedby="basic-addon2" name="stock_count"
                                                value="{{$product->stock_count}}">
                                                <div class="input-group-append">
                                                <button type="submit" class="btn btn-success" type="button">Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                    {{-- <td>$ {{$product->price}}</td> --}}
                                    @can('edit_product')     
                                    <td>
                                            <button class="btn btn-warning btn-sm" onclick="editProduct({{$product->id}})">Edit</button>                                       
                                    </td>
                                    @endcan
                                </tr>
                                @php $x++; @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">No records available</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    {{--    add product modal--}}
    <div class="modal fade" id="add_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <form action="{{route('admin.product.save')}}" method="POST" enctype="multipart/form-data">

            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{csrf_field()}}
                        <div class="form-group ">
                            <label for="name">Supplier *</label>
                            <select name="supplier_code" id="supplier_code" class="form-control select2" required>
                                <option value="{{$currentSupplier->supplier_code}}">{{$currentSupplier->supplier_code}}
                                    - {{$currentSupplier->name}}</option>
                                {{--                                @if(count($suppliers))--}}
                                {{--                                    @foreach($suppliers as $supplier)--}}
                                {{--                                        <option value="{{$supplier->supplier_code}}">{{$supplier->supplier_code}} - {{$supplier->name }}</option>--}}
                                {{--                                    @endforeach--}}
                                {{--                                @endif--}}
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="product_id">Product ID *</label>
                            <input type="text" id="product_id" name="product_id" class="form-control"
                                   value="{{old('product_id')}}" required="" onkeyup="checkProductIdExists('product_id')">
                            <div id="product_id_msg"></div>
                        </div>
                        <div class="form-group ">
                            <label for="product_image">Product Image </label>
                            <input type="file" id="product_image" name="product_image" class="form-control" value=""
                            >
                        </div>
                        <div class="form-group ">
                            <label for="product_name">product Name *</label>
                            <input type="text" id="product_name" name="product_name" class="form-control"
                                   value="{{old('product_name')}}" required="">
                        </div>
                        <div class="form-group ">
                            <label for="stock_count">Stock Count *</label>
                            <input type="number" id="stock_count" name="stock_count" class="form-control"
                                   value="{{old('stock_count')}}" required="">
                        </div>
                        {{-- <div class="form-group ">
                            <label for="price">Price *</label>
                            <input type="number" id="price" name="price" class="form-control"
                                   value="{{old('price')}}" required="">
                        </div> --}}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="product_id_btn">Save changes</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
    {{--    END add product modal--}}

    {{--    Edit product modal--}}
    <div class="modal fade" id="edit_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <form action="{{route('admin.product.update')}}" method="POST" enctype="multipart/form-data">

            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{csrf_field()}}
                        <input type="hidden" name="id" id="id">
                        <div class="form-group ">
                            <label for="name">Supplier *</label>
                            <select name="supplier_code" id="supplier_code" class="form-control select2" required>
                                <option value="{{$currentSupplier->supplier_code}}">{{$currentSupplier->supplier_code}}
                                    - {{$currentSupplier->name}}</option>
                                {{--                                @if(count($suppliers))--}}
                                {{--                                    @foreach($suppliers as $supplier)--}}
                                {{--                                        <option value="{{$supplier->supplier_code}}">{{$supplier->supplier_code}} - {{$supplier->name }}</option>--}}
                                {{--                                    @endforeach--}}
                                {{--                                @endif--}}
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="product_id">Product ID *</label>
                            <input type="text" id="product_id_edit" name="product_id" class="form-control"
                                   value="{{old('product_id')}}" required="" {{$readOnly}} onkeyup="checkProductIdExists('product_id_edit')">
                            <div id="product_id_edit_msg"></div>
                        </div>
                        <div class="form-group ">
                            <label for="product_image_edit">Product Image </label>
                            <div id="product_img_div" >
                                <small style="display: block;
                                margin-bottom: 5px;">Saved Image</small>
                                <img src="" alt="" id="product_img_tag_old" width="100px" height="auto">
                                <small style="display: block;
                                margin-top: 5px; margin-bottom:5px;    color: #e69720;">Select a new image to replace existing image.</small>
                            </div>
                            @if ($readOnly == '')                                
                            <input type="file" id="product_image_edit" name="product_image" class="form-control" value="">
                            @endif
                            <input type="hidden" name="product_image_old" id="product_image_old">

                        </div>
                        <div class="form-group ">
                            <label for="product_name_edit">product Name *</label>
                            <input type="text" id="product_name_edit" name="product_name" class="form-control"
                                   value="{{old('product_name')}}" required="" {{$readOnly}}>
                        </div>
                        <div class="form-group ">
                            <label for="stock_count_edit">Stock Count *</label>
                            <input type="number" id="stock_count_edit" name="stock_count" class="form-control"
                                   value="{{old('stock_count')}}" required="">
                        </div>
                        {{-- <div class="form-group ">
                            <label for="price_edit">Price *</label>
                            <input type="number" id="price_edit" name="price" class="form-control"
                                   value="{{old('price')}}" required="">
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="product_id_edit_btn">Update changes</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
    {{--    END Edit product modal--}}


    {{--    Import CSV modal--}}
    <div class="modal fade" id="import_csv" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">

        <form action="{{route('admin.csv.import')}}" method="POST" enctype="multipart/form-data">

            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Add Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{csrf_field()}}
                        <div class="form-group ">
                            <a href="{{asset('import/demo.csv')}}" class="btn btn-block btn-primary">Download CSV/Excel
                                Import Sample File</a>
                        </div>
                        <div class="form-group ">
                            <label for="name">Supplier *</label>
                            <select name="supplier_code" id="supplier_code" class="form-control select2" required>
                                <option value="{{$currentSupplier->supplier_code}}">{{$currentSupplier->supplier_code}}
                                    - {{$currentSupplier->name}}</option>
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="product_image">CSV/Excel File *</label>
                            <input type="file" id="file" name="file" class="form-control" value=""
                                   required="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
    {{--    END Import CSV modal--}}


@endsection

@section('scripts')
    @parent
    <script>
        var asset_path = "{{asset('')}}";
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

            $.extend(true, $.fn.dataTable.defaults, {
                order: [[1, 'desc']],
                pageLength: 100,
            });
            $('.datatable-products:not(.ajaxTable)').DataTable({buttons: dtButtons})
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                    .columns.adjust();
            });
        });

        function websiteUpdate(count,supplierCode) {
            Swal.fire({
                title: 'Do you wish to update the website?',
                text: 'There are '+count+' record(s) to be updated!',
                showCancelButton: true,
                confirmButtonText: 'Save',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {'x-csrf-token': _token},
                        method: 'POST',
                        url: '/admin/update/website',
                        data: { supplier_code: supplierCode}})
                        .done(function (res) {
                            console.log(res);
                            if (res.status){
                                var msg = 'Website Updated successfully! ' + res.count + ' record(s) has been updated!';
                                $('#js_msg').html('<div class="alert alert-success" role="alert">'+msg+'</div>');

                                setTimeout(function (e){
                                    location.reload();
                                },1500);
                            }
                        })
                }
            });
        }

        function supplierUpdate(count,supplierCode) {
            Swal.fire({
                title: 'Do you wish to publish?',
                text: 'There are '+count+' record(s) to be updated!',
                showCancelButton: true,
                confirmButtonText: 'Save',
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        headers: {'x-csrf-token': _token},
                        method: 'POST',
                        url: '/admin/update/supplier',
                        data: { supplier_code: supplierCode}})
                        .done(function (res) {
                            console.log(res);
                            if (res.status){
                                var msg = 'Data published successfully! ' + res.count + ' record(s) has been updated!';
                                $('#js_msg').html('<div class="alert alert-success" role="alert">'+msg+'</div>');

                                setTimeout(function (e){
                                    location.reload();
                                },1500);
                            }
                        })
                }
            });
        }
        

        function editProduct(id) {
            $('#product_img_div').hide();
            $('#product_img_tag_old').hide();
            $.ajax({
                headers: {'x-csrf-token': _token},
                method: 'POST',
                url: '/admin/product/single/get',
                data: {id: id, _method: 'POST'}
            })
                .done(function (res) {
                    if(res.product_image != null || res.product_image != '' ){
                        $('#product_img_tag_old').show().attr("src",asset_path+res.product_image);
                        $('#product_img_div').show();
                    }
                    $('#id').val(res.id);
                    $('#product_id_edit').val(res.product_id);
                    $('#product_image_old').val(res.product_image);
                    $('#product_name_edit').val(res.product_name);
                    $('#stock_count_edit').val(res.stock_count);
                    $('#edit_product').modal('show');
                    $('#product_id_edit_btn').prop('disabled', false);
                    $('#product_id_edit_msg').html("");
                })
        }

        function checkProductIdExists(element_id){
            var product_id = $('#'+element_id).val();
            $('#'+element_id+'_btn').prop('disabled', true);
            $('#'+element_id+'_msg').html("");
            console.log(product_id);
            if(product_id != ''){
                setTimeout(function(){
                    $.ajax({
                        headers: {'x-csrf-token': _token},
                        method: 'POST',
                        url: '/admin/check/product-id',
                        data: { product_id: product_id}
                    }).done(function (res) {
                        if(res){
                            $('#'+element_id+'_btn').prop('disabled', false);
                        }else{
                            $('#'+element_id+'_btn').prop('disabled', true);
                            $('#'+element_id+'_msg').html("<p class='error-msg'>Product id already exist!</p>");
                        }
                        
                    });
                },1000);
            }
        }
    </script>
@endsection

@section('styles')
<style>
    .error-msg{
    font-size: 12px;
    color: #ed0808;
    }
</style>

@endsection