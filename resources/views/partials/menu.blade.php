<div class="sidebar">
    <nav class="sidebar-nav">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            @if(\Illuminate\Support\Facades\Gate::allows('users_manage') || \Illuminate\Support\Facades\Gate::allows('user_add'))
                <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle" href="#">
                        <i class="fa-fw fas fa-users nav-icon">

                        </i>
                        {{ trans('cruds.userManagement.title') }}
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-unlock-alt nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-briefcase nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-user nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            <li class="nav-item">
                <a href="{{ route('auth.change_password') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-key">

                    </i>
                    Change password
                </a>
            </li>
            @can('edit_settings')
            <li class="nav-item">
                <a href="{{ route('admin.settings.general') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-cog">

                    </i>
                   Settings
                </a>
            </li>
            @endcan

            @can('admin_activity_log')
            <li class="nav-item">
                <a href="{{ route('admin.activitylog.view') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-cog">

                    </i>
                   Activity Log
                </a>
            </li>
            @endcan

            @can('user_activity_log')
            <li class="nav-item">
                <a href="{{ route('admin.activitylog.view') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-cog">

                    </i>
                   Activity Log
                </a>
            </li>
            @endcan
           
            {{--<li class="nav-item">--}}
                {{--<a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">--}}
                    {{--<i class="nav-icon fas fa-fw fa-sign-out-alt">--}}

                    {{--</i>--}}
                    {{--{{ trans('global.logout') }}--}}
                {{--</a>--}}
            {{--</li>--}}
        </ul>

    </nav>

</div>