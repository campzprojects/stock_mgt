@extends('layouts.admin')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <h2>Dashboard</h2>
            </div>
        </div>
        {{--    admin dashboard--}}
        @can('admin_dashboard')
            <br>
            <div class="row">
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.permissions.create") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="medkit-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">Add Permission</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.permissions.index") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="list-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">List Permission</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.roles.create") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="id-card-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">Add Roles</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.roles.index") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="finger-print-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">List Roles</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="row">
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.users.create") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="person-add-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">Add User</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.users.index") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="people-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">List Users</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="/admin/users?type=Admin"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="people-circle-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">List Admins</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.supplier.list") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="storefront-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">List Suppliers</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{ route("admin.supplier.create") }}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="people-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">Add Supplier</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>


        @endcan
        {{--   END admin dashboard--}}

        {{--  manager dashboard--}}
        @can('manager_dashboard')

            <div class="col-6 col-sm-4 col-md-3">
                <a href="{{ route("admin.supplier.create") }}"
                   class="">
                    <div class="card mn200 ybox ">
                        <div class="media">
                            <p class="dashnumber">
                                <ion-icon name="people-outline"></ion-icon>
                            </p>
                            <div class="media-body">
                                <h5 class="dashhead">Add Supplier</h5>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-6 col-sm-4 col-md-3">
                <a href="{{ route("admin.supplier.list") }}"
                   class="">
                    <div class="card mn200 ybox ">
                        <div class="media">
                            <p class="dashnumber">
                                <ion-icon name="storefront-outline"></ion-icon>
                            </p>
                            <div class="media-body">
                                <h5 class="dashhead">List Suppliers</h5>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
        @endcan
        {{--   END manager dashboard--}}
        
        {{--    supplier dashboard--}}
        @can('supplier_dashboard')
            <br>
            <div class="row">
                <div class="col-6 col-sm-4 col-md-3">
                    <a href="{{route('admin.supplier.single',['id'=>\Illuminate\Support\Facades\Auth::user()->id])}}"
                       class="">
                        <div class="card mn200 ybox ">
                            <div class="media">
                                <p class="dashnumber">
                                    <ion-icon name="storefront-outline"></ion-icon>
                                </p>
                                <div class="media-body">
                                    <h5 class="dashhead">Data Population</h5>

                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endcan
        {{--   END supplier dashboard--}}

         {{--    supplier dashboard--}}
         @can('data_entry_dashboard')
         <br>
         <div class="row">
            <div class="col-6 col-sm-4 col-md-3">
                <a href="{{ route("admin.supplier.list") }}"
                   class="">
                    <div class="card mn200 ybox ">
                        <div class="media">
                            <p class="dashnumber">
                                <ion-icon name="storefront-outline"></ion-icon>
                            </p>
                            <div class="media-body">
                                <h5 class="dashhead">List Suppliers</h5>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
         </div>
     @endcan
     {{--   END supplier dashboard--}}


    </div>
@endsection
@section('scripts')
    @parent

@endsection